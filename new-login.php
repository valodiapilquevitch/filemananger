                                <form class="form-signin" action="" method="post" autocomplete="off">

                                    <div class="form-group">

                                        <label for="fm_usr"><?php echo lng('Username'); ?></label>

                                        <input type="text" class="form-control" id="fm_usr" name="fm_usr" required autofocus>

                                    </div>



                                    <div class="form-group">

                                        <label for="fm_pwd"><?php echo lng('Password'); ?></label>

                                        <input type="password" class="form-control" id="fm_pwd" name="fm_pwd" required>

                                    </div>



                                    <div class="form-group">

                                        <div class="custom-checkbox custom-control">

                                            <input type="checkbox" name="remember" id="remember" class="custom-control-input">

                                            <label for="remember" class="custom-control-label"><?php echo lng('RememberMe'); ?></label>

                                        </div>

                                    </div>



                                    <div class="form-group">

                                        <button type="submit" class="btn btn-success btn-block" role="button">

                                            <?php echo lng('Login'); ?>

                                        </button>

                                    </div>

                                </form>